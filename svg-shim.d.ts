declare module '*.svg' {

    import { Component } from 'vue';

    const content: Component;
    export default content;
}
