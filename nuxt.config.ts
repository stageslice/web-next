import { Configuration } from '@nuxt/types';
import { Configuration as WebpackConfiguration } from 'webpack';

require('dotenv').config();

const config: Configuration = {
  mode: 'universal',
  env: {
    baseUrl: process.env.BASE_URL || 'http://localhost:3000',
    apiEndpoint: process.env.API_ENDPOINT || 'http://localhost:3000'
  },
  /*
            ** Headers of the page
            */
  head: {
    title: 'Stage Slice',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  router: {},
  /*
            ** Customize the progress-bar color
            */
  loading: { color: '#fff' },
  /*
            ** Global CSS
            */
  css: [],
  /*
            ** Plugins to load before mounting the App
            */
  plugins: [
    '@/plugins/vue-lazyload.js'
  ],
  /*
            ** Nuxt.js dev-modules
            */
  buildModules: [
    '@nuxtjs/dotenv',
    '@nuxt/typescript-build',
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    // Doc: https://github.com/nuxt-community/nuxt-tailwindcss
    '@nuxtjs/tailwindcss',
    '@nuxtjs/apollo',
    'portal-vue/nuxt'
  ],

  apollo: {
    // required
    clientConfigs: {
      default: {
        // required
        httpEndpoint: process.env.API_ENDPOINT + '/api/graphql',
        // You can use `wss` for secure connection (recommended in production)
        // Use `null` to disable subscriptions
        wsEndpoint: null, // optional
        // LocalStorage token
        tokenName: 'apollo-token', // optional
        // Enable Automatic Query persisting with Apollo Engine
        persisting: false, // Optional
        // Use websockets for everything (no HTTP)
        // You need to pass a `wsEndpoint` for this to work
        websocketsOnly: false // Optional
      }
    }
  },
  pwa: {
    manifest: {
      background_color: '#282C38',
      theme_color: '#54AAC8'
    }
  },
  /*
            ** Nuxt.js modules
            */
  modules: [
    '@nuxtjs/pwa',
    '@nuxtjs/proxy'
  ],
  proxy: {
    // Simple proxy
    '/api': {
      target: process.env.API_ENDPOINT
    }
  },
  /*
            ** Build configuration
            */
  build: {
    extend (config: WebpackConfiguration) {
      // @ts-ignore
      const svgRule = config.module.rules.find(rule => rule.test.test('.svg'));
      // @ts-ignore
      svgRule.test = /\.(png|jpe?g|gif|webp)$/;
      // @ts-ignore
      config.module.rules.push({
        test: /\.svg$/,
        use: ['babel-loader', 'vue-svg-loader']
      });
    }

  }
};

export default config;
