/*
** TailwindCSS Configuration File
**
** Docs: https://tailwindcss.com/docs/configuration
** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
*/
module.exports = {
  theme: {
    extend: {
      colors: {
        t: {
          '50-background': 'rgba(51,56,70,0.5)'
        },
        font: {
          primary: '#f8fafc',
          secondary: '#919fab',
          'secondary-dark': '#697682'
        },

        brand: {
          main: '#497A91',
          accent: '#54AAC8'
        },

        background: {
          light: '#4b5162',
          default: '#333846',
          dark: '#252A38',
          darker: '#282C38'
        }
      },
      spacing: {
        full: '100%',
        'x1/2': '150%',
        x2: '200%'
      },
      maxWidth: {
        xxxs: '12rem',
        xxs: '16rem'
      },
      width: {
        em: '1em'
      },
      height: {
        em: '1em'
      }
    }
  },
  variants: {},
  plugins: []
};
