module.exports = {
  apps: [{
    name: 'StageSlice',
    script: './node_modules/nuxt-start/bin/nuxt-start.js',
    // Options reference: https://pm2.io/doc/en/runtime/reference/ecosystem-file
    instances: 2,
    cwd: './current',
    port: 3000,
    exec_mode: 'cluster',
    env: {
      NODE_ENV: 'development'
    },
    env_production: {
      NODE_ENV: 'production'
    }
  }]
}
